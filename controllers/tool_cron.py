# coding: utf8

import common

if False:
    from gluon import *
    import db

    request = current.request
    response = current.response
    session = current.session
    cache = current.cache
    T = current.T

session.forget(response)

# запустим сразу защиту от внешних вызов
# see "trust_ip" in private/appconfig.ini
common.not_is_local(request)


def index():
    return dict(message="use CRON for this controller")


# take external rates for cron
def get_ext_rates():
    import serv_rates
    serv_rates.get_rates(db, -1)
    db.commit()  # need for CRON
    return 'OK'


def proc_history():
    import serv_to_buy
    mess = serv_to_buy.proc_history(db, only_list=None, ed_acc=None, from_dt_in='same', test=False)
    db.commit()  # need for CRON
    return mess
