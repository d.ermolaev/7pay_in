# -*- coding: utf-8 -*-

import common

if False:
    from gluon import *
    import db

    request = current.request
    response = current.response
    session = current.session
    cache = current.cache
    T = current.T

session.forget(response)

# запустим сразу защиту от внешних вызов
# see "trust_ip" in private/appconfig.ini
common.not_is_local(request)


# попробовать что-либо вида
def index(): return dict(message="hello from test.py")


def a():
    mess = '7pb4'
    print (mess[0:3])
    if mess.startswith('7pb'):
        # просто код заказа тут
        order_id = mess[3:]
        order_id = mess.split('7pb')[1]
        print (order_id)
        if order_id.isdigit():
            order = db.addr_orders[order_id]
            if order:
                xcurr = db.xcurrs[order.xcurr_id]
                addr = order.addr
                if xcurr: return addr
    return 'ddd'
