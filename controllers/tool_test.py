# coding: utf8

import common

if False:
    from gluon import *
    import db

    request = current.request
    response = current.response
    session = current.session
    cache = current.cache
    T = current.T

session.forget(response)

# запустим сразу защиту от внешних вызов
# see "trust_ip" in private/appconfig.ini
common.not_is_local(request)

current.logger = logger = logging.getLogger('test')  ## [logger_debug] see private/logconfig.ini


def index():
    return dict(message="test with rollback db. Use block_proc/CURR/BLOCK_NO. Logs see in test.log")


## TEST and ROLLBACK DB
def block_proc():
    if len(request.args) < 2:
        mess = 'set CUR and Block as /block_proc/ETH/653432'
        return mess

    import serv_block_proc
    abbrev = request.args[0]
    block_test = request.args[1]
    result = serv_block_proc.run_once(db, abbrev, int(block_test), test=True)
    db.rollback()
    return 'Logs see in test.log<br>' + result


## TEST and ROLLBACK DB
def serv_to_pay_xcurr():
    if len(request.args) < 1:
        mess = 'set CUR as /block_proc/ETH'
        return mess

    import serv_to_pay, db_common
    abbrev = request.args[0]
    curr_in, xcurr, e = db_common.get_currs_by_abbrev(db, abbrev)
    result = serv_to_pay.proc_xcurr(db, curr_in, xcurr, 1, test=True)
    db.rollback()
    return 'Logs see in test.log<br>'
