# -*- coding: utf-8 -*-


from decimal import Decimal
import decimal
import db_common, crypto_client, rpc_ethereum_geth

import common

if False:
    from gluon import *
    import db

    request = current.request
    response = current.response
    session = current.session
    cache = current.cache
    T = current.T

session.forget(response)

# запустим сразу защиту от внешних вызов
# see "trust_ip" in private/appconfig.ini
common.not_is_local(request)

# запустим сразу защиту от внешних вызов
# тут только то что на локалке TRUST_IP in private/appconfig.ini
common.not_is_local(request)


def index(): return dict(
    message="USE: add_system, addrs, bal, block, dehex, enhex, get_txs, get_unc_incomes, index, send, wei")


def wei():
    str = "0xe043da617250008"
    decimal.getcontext().prec = 18
    return dict(int=int(str, 0), decimal=Decimal(int(str, 0)), decimal18=Decimal(int(str, 0)) * Decimal(1E-18))


# get Address of wallet
def addrs():
    curr, xcurr, e = db_common.get_currs_by_abbrev(db, 'ETH')
    if not xcurr:
        return 'xcurr not found'

    token_key = xcurr.as_token
    token = db.tokens[token_key]
    token_system = db.systems[token.system_id]

    json = rpc_ethereum_geth.rpc_request(token_system.connect_url, 'eth_accounts')
    # return xcurr.connect_url + (' %s' % json)
    return json


# Test balance for Address
def bal():
    curr, xcurr, e = db_common.get_currs_by_abbrev(db, 'ETH')
    if not xcurr:
        return 'xcurr not found'

    token_key = xcurr.as_token
    token = db.tokens[token_key]
    token_system = db.systems[token.system_id]

    if request.args(0):
        address = request.args(0)
    else:
        address = token_system.account

    res = rpc_ethereum_geth.get_balance(token_system, address)

    return BEAUTIFY(dict(address=address, res=res))


def send():
    curr, xcurr, e = db_common.get_currs_by_abbrev(db, 'ETH')
    if not xcurr:
        return 'xcurr not found'

    nonce = request.vars.get('nonce')
    if nonce == None:  ## 0 it not None
        return 'please set ?nonce=NN or -'

    token_key = xcurr.as_token
    token = db.tokens[token_key]
    token_system = db.systems[token.system_id]

    if request.args(0):
        toAddress = request.args(0)
    else:
        toAddress = '0x87025A01969FB46337876D8b73840Fa05890Be78'

    mess = 'erachain.org & foil.network'
    password = token_system.password

    nonce = None
    try:
        nonce = int(nonce)
    except:
        pass

    res, bal = rpc_ethereum_geth.send(curr, xcurr, toAddress, Decimal(0.001), token_system, nonce=nonce, mess=mess,
                                      sender=None, password=password)

    return BEAUTIFY(dict(res=res, bal=bal))


def block():
    curr, xcurr, e = db_common.get_currs_by_abbrev(db, 'ETH')
    if not xcurr:
        return 'xcurr not found'

    if request.args(0):
        block = request.args(0)
        try:
            block = '%#x' % int(block)
        except:
            pass
    else:
        block = '0x100'

    token_key = xcurr.as_token
    token = db.tokens[token_key]
    token_system = db.systems[token.system_id]

    return BEAUTIFY(rpc_ethereum_geth.get_block(token_system.connect_url, block))


def enhex():
    if request.args(0):
        return request.args(0).encode("utf-8").hex()
    ##return PY2 '0x' + (u'ETH from erachain.org stablecoin'.encode("hex"))
    return '0x' + (u'ETH ПРИВЕТ erachain.org stablecoin'.encode("utf-8").hex())


def dehex():
    import codecs
    if request.args(0):
        return codecs.decode(request.args(0)[2:], "hex_codec").decode("utf-8")
    return codecs.decode(u'ETH ПРИВЕТ erachain.org stablecoin'.encode("utf-8").hex(), "hex_codec").decode("utf-8")


def add_system():
    name = 'ethereum'
    system_id = db.systems.insert(name='Ethereum', name2=name, first_char='0x',
                                  connect_url='http://localhost:8545',  # for Testnet use http://127.0.0.1:9068
                                  password='123456789',
                                  block_time=60, conf=2, conf_gen=64,
                                  from_block=1000000  # for Testnet use 0
                                  )
    for asset in [
        [1, 'ETH']
    ]:
        token_id = db.tokens.insert(system_id=system_id, token_key=asset[0], name=asset[1])
        curr_id = db.currs.insert(abbrev=asset[1], name=asset[1], name2=asset[1], used=True)
        db.xcurrs.insert(curr_id=curr_id, protocol='', connect_url=name + ' ' + asset[1],
                         as_token=token_id,
                         block_time=0, txfee=0, conf=0, conf_gen=0)


def get_txs():
    curr, xcurr, e = db_common.get_currs_by_abbrev(db, 'ETH')
    if not xcurr:
        return 'xcurr not found'

    token_key = xcurr.as_token
    token = db.tokens[token_key]
    token_system = db.systems[token.system_id]
    recs, height = crypto_client.get_transactions(xcurr, token_system,
                                                  request.args(0) and int(request.args(0)) or token_system.from_block)
    return BEAUTIFY({'recs': recs, 'height': height})


def get_unc_incomes():
    curr, xcurr, e = db_common.get_currs_by_abbrev(db, 'ETH')
    if not xcurr:
        return 'xcurr not found'

    token_key = xcurr.as_token
    token = db.tokens[token_key]
    token_system = db.systems[token.system_id]
    recs = rpc_ethereum_geth.get_unconf_incomes(token_system.connect_url, token_system.account)
    return BEAUTIFY(recs)


def peers():
    curr, xcurr, e = db_common.get_currs_by_abbrev(db, 'ETH')
    if not xcurr:
        return 'xcurr not found'

    token_key = xcurr.as_token
    token = db.tokens[token_key]
    token_system = db.systems[token.system_id]
    return BEAUTIFY(rpc_ethereum_geth.rpc_request(token_system.connect_url, 'admin_peers'))


def seek_txpool():
    curr, xcurr, e = db_common.get_currs_by_abbrev(db, 'ETH')
    if not xcurr:
        return 'xcurr not found'

    if len(request.args) == 0:
        return 'use: /tx_hash'

    find = request.args(0).lower()

    token_key = xcurr.as_token
    token = db.tokens[token_key]
    token_system = db.systems[token.system_id]
    res = rpc_ethereum_geth.rpc_request(token_system.connect_url, 'txpool_content')

    out = {'pending': {}, 'queued': {}}

    items = res['result']['pending']
    for addrFrom, txs in items.items():
        for k, tx in txs.items():
            if tx['hash'] == find:
                out['pending'][k] = tx

    items = res['result']['queued']
    for addrFrom, txs in items.items():
        for k, tx in txs.items():
            if tx['hash'] == find:
                out['queued'][k] = tx

    return BEAUTIFY(out)


def addr_txpool():
    curr, xcurr, e = db_common.get_currs_by_abbrev(db, 'ETH')
    if not xcurr:
        return 'xcurr not found'

    if len(request.args) == 0:
        return 'use: /from_address'

    find = request.args(0).lower()

    token_key = xcurr.as_token
    token = db.tokens[token_key]
    token_system = db.systems[token.system_id]
    res = rpc_ethereum_geth.rpc_request(token_system.connect_url, 'txpool_content')

    out = {'pending': {}, 'queued': {}}

    items = res['result']['pending']
    for addrFrom, txs in items.items():
        for k, tx in txs.items():
            if tx['from'] == find:
                out['pending'][k] = tx

    items = res['result']['queued']
    for addrFrom, txs in items.items():
        for k, tx in txs.items():
            if tx['from'] == find:
                out['queued'][k] = tx

    return BEAUTIFY(out)

# 0x000223f245958443c59EaA9E867799B996D357f0
# 0x000223f245958443c59eaa9e867799b996d357f0
def addr_txpool2():
    curr, xcurr, e = db_common.get_currs_by_abbrev(db, 'ETH')
    if not xcurr:
        return 'xcurr not found'

    if len(request.args) == 0:
        return 'use: /from_address'

    find = request.args(0)
    find_key = bytes.fromhex(find)

    token_key = xcurr.as_token
    token = db.tokens[token_key]
    token_system = db.systems[token.system_id]
    res = rpc_ethereum_geth.rpc_request(token_system.connect_url, 'txpool_content')
    pending = res['pending']
    queued = res['queued']

    out = {'pending': pending[find_key], 'queued': queued[find_key]}

    return BEAUTIFY(out)


def txpool():
    curr, xcurr, e = db_common.get_currs_by_abbrev(db, 'ETH')
    if not xcurr:
        return 'xcurr not found'

    token_key = xcurr.as_token
    token = db.tokens[token_key]
    token_system = db.systems[token.system_id]
    res = rpc_ethereum_geth.rpc_request(token_system.connect_url, 'txpool_content')

    return BEAUTIFY(res['result'])


## use with 'gas'
def gas_price():
    curr, xcurr, e = db_common.get_currs_by_abbrev(db, 'ETH')
    if not xcurr:
        return 'xcurr not found'

    token_key = xcurr.as_token
    token = db.tokens[token_key]
    token_system = db.systems[token.system_id]
    res = rpc_ethereum_geth.rpc_request(token_system.connect_url, 'eth_gasPrice')

    return BEAUTIFY(res['result'])


## use with 'maxFeePerGas'
def max_priority_fee_per_gas():
    curr, xcurr, e = db_common.get_currs_by_abbrev(db, 'ETH')
    if not xcurr:
        return 'xcurr not found'

    token_key = xcurr.as_token
    token = db.tokens[token_key]
    token_system = db.systems[token.system_id]
    res = rpc_ethereum_geth.rpc_request(token_system.connect_url, 'eth_maxPriorityFeePerGas')

    return BEAUTIFY(res['result'])


def estimate_gas():
    curr, xcurr, e = db_common.get_currs_by_abbrev(db, 'ETH')
    if not xcurr:
        return 'xcurr not found'

    token_key = xcurr.as_token
    token = db.tokens[token_key]
    token_system = db.systems[token.system_id]
    res = rpc_ethereum_geth.rpc_request(token_system.connect_url, 'eth_estimateGas',
                                        [{"from": "0xe699ac988f1275d78699D1deA99bE0EF15a91F03", \
                                          "to": "0x87025A01969FB46337876D8b73840Fa05890Be78", "value": "0x8700000"}])

    return BEAUTIFY(res['result'])
