# coding: utf8

from time import sleep
import crypto_client

if False:
    from gluon import *
    request = current.request
    response = current.response
    session = current.session
    cache = current.cache
    T = current.T
    db = None

session.forget(response)


def help():
    redirect(URL('index'))


@cache.action(time_expire=300, cache_model=cache.disk) #, vars=False, public=True, lang=True)
def index():
    return dict()


def tx_exist():

    txid = request.args(1)
    if not txid:
        return {'error':"need txid: /tx_info.json/[curr]/[txid]"}
    curr_abbrev = request.args(0)
    import db_common
    curr,xcurr,e = db_common.get_currs_by_abbrev(db, curr_abbrev)
    if not xcurr:
        return {"error": "invalid curr:  /tx_info.json/[curr]/[txid]"}

    sleep(1)

    conn = None
    token_key = xcurr.as_token
    token = token_key and db.tokens[token_key]
    token_system = token and db.systems[token.system_id]
    return dict(result=crypto_client.tx_exist_in_chain(xcurr, token_system, txid, conn))


def tx_seqno():

    txid = request.args(1)
    if not txid:
        return {'error':"need txid: /tx_info.json/[curr]/[txid]"}
    curr_abbrev = request.args(0)
    import db_common
    curr,xcurr,e = db_common.get_currs_by_abbrev(db, curr_abbrev)
    if not xcurr:
        return {"error": "invalid curr:  /tx_info.json/[curr]/[txid]"}

    sleep(1)

    conn = None
    token_key = xcurr.as_token
    token = token_key and db.tokens[token_key]
    token_system = token and db.systems[token.system_id]
    return dict(result=crypto_client.get_tx_seq_no(xcurr, token_system, txid, conn))

def tx():
    if request.extension == 'html':
        response.view = 'blockchain/res.html'

    txid = request.args(1)
    if not txid:
        return {'error':"need txid: /tx_info.json/[curr]/[txid]"}
    curr_abbrev = request.args(0)
    import db_common
    curr,xcurr,e = db_common.get_currs_by_abbrev(db, curr_abbrev)
    if not xcurr:
        return {"error": "invalid curr:  /tx_info.json/[curr]/[txid]"}

    sleep(1)

    conn = None
    token_key = xcurr.as_token
    if token_key:
        token =  db.tokens[token_key]
        token_system = db.systems[token.system_id]
        res = dict(result=crypto_client.get_tx_info(xcurr, token_system, txid, conn))
        return res

    conn = crypto_client.connect(curr, xcurr)
    if not conn:
        return {"error": "not connected to wallet"}

    res = None
    try:
        res = conn.gettransaction(txid)  # выдает только из своего кошелька
    except:
        pass

    if not res or 'details' not in res:
        try:
            res = conn.getrawtransaction(txid, 1)  # все выдает
        except Exception as e:
            return {'error': e}

    if 'hex' in res: res.pop('hex')
    txid = res.get('txid')
    if txid: res['txid'] = A(txid, _href=URL('tx',args=[request.args(0), txid]))
    for inp in res.get('vin',[]):
        if 'scriptSig' in inp: inp.pop('scriptSig')
        txid = inp.get('txid')
        if txid:
            inp['txid'] = A(txid, _href=URL('tx',args=[request.args(0), txid]))
        else:
            pass
        pass
    return res
