# coding: utf8

from gluon import *

import sys
#PRINT_AS_FUNC and print(sys.argv) or print (sys.argv)

if False:
    import db
    request = current.request
    response = current.response
    session = current.session
    cache = current.cache
    T = current.T

from time import sleep
import datetime
from threading import Thread

try:
    import json
except ImportError:
    from gluon.contrib import simplejson as json
from gluon.tools import fetch

# запуск опроса бирж и получение от них цен
import exch_client
import db_common
import clients_lib
import rates_lib


def get_ticker(db, exchg_id, curr_id):
    curr = db.currs[curr_id]
    if curr.used:
        limits = db_common.get_limits(db, exchg_id, curr_id)
        return limits and limits.ticker or curr.abbrev.lower()
def get_curr(db, exchg_id, ticker):
    limit = db((db.exchg_limits.exchg_id==exchg_id) & (db.exchg_limits.ticker==ticker)).select().first()
    return limit and db.currs[limit.curr_id] or db(db.currs.abbrev==ticker.upper()).select().first()

def from_livecoin(db, exchg):
    exchg_id = exchg.id
    for pair in db_common.get_exchg_pairs(db, exchg_id):
        if not pair.used: continue
        t1 = get_ticker(db, exchg_id, pair.curr1_id)
        t2 = get_ticker(db, exchg_id, pair.curr2_id)
        if not t1 or not t2:
            continue

        t1 = t1.upper()
        t2 = t2.upper()
        current.logger.info(t1 + ':' + t2) # 'pair.ticker:', pair.ticker,

        try:
            #if True:
            '''
            https://api.livecoin.net/exchange/ticker?currencyPair=EMC/BTC
            {"last":0.00051000,"high":0.00056000,"low":0.00042690,"volume":21150.15056000,
                "vwap":0.00049384964581547641,"max_bid":0.00056000,"min_ask":0.00042690,"best_bid":0.00046000,"best_ask":0.00056960} '''
            cryp_url = 'https://' + exchg.url + '/' + exchg.API + '?currencyPair=' + t1 + '/' + t2
            #current.logger.debug(cryp_url)
            res = fetch(cryp_url)
            ## res = {"best_bid":0.00046000,"best_ask":0.00056960}
            res = json.loads(res)
            if type(res) != dict:
                continue
            if not res.get('best_bid'): continue
            buy = float(res['best_ask'])
            sell = float(res['best_bid'])
            current.logger.debug('sell: %s, buy: %' % (sell, buy))
            db_common.store_rates(db, pair, sell, buy)
        except Exception as e:
            current.logger.exception(e)
            continue
    db.commit()

def from_poloniex(db, exchg):
    exchg_id = exchg.id
    for pair in db_common.get_exchg_pairs(db, exchg_id):
        if not pair.used or not pair.ticker:
            continue

        t1 = get_ticker(db, exchg_id, pair.curr1_id)
        t2 = get_ticker(db, exchg_id, pair.curr2_id)
        if not t1 or not t2:
            continue

        '''
        https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_DOGE
    [{"globalTradeID":13711923,"tradeID":469269,"date":"2016-01-17 11:37:16","type":"sell",
    "rate":"0.00000039","amount":"273.78000000","total":"0.00010677"},        '''
        #current.logger.debug(t1 + ':' + t2 + ' pair.ticker: ' + pair.ticker)

        try:
            #if True:
            #params = {'method': 'singlemarketdata', 'marketid': pair.ticker }
            #cryp_url = 'https://' + exchg.url + '/public?command=returnTradeHistory&currencyPair=' + pair.ticker
            cryp_url = 'https://' + exchg.url + '/public?command=returnOrderBook&depth=1&currencyPair=' + pair.ticker
            ## res = {"asks":[["0.00001852",59.39524844]],"bids":[["0.00001851",710.99297675]],"isFrozen":"0"}
            #current.logger.debug(cryp_url)
            res = fetch(cryp_url)
            res = json.loads(res)
            if type(res) != dict:
                continue
            if not res.get('isFrozen'): continue
            if pair.ticker == 'USDT_BTC' or pair.ticker == 'USDT_ETH':
                # BACK RATE
                buy = float(res['asks'][0][0])
                sell = float(res['bids'][0][0])
            else:
                # v1
                sell = 1/float(res['asks'][0][0])
                buy = 1/float(res['bids'][0][0])
            #return dict(buy= buy, sell= sell)
            current.logger.info('sell: %s, buy: %s' % (sell, buy))
            db_common.store_rates(db, pair, sell, buy)
        except Exception as e:
            current.logger.exception(e)
            continue
    db.commit()

def from_cryptsy(db, exchg):
    exchg_id = exchg.id
    for pair in db_common.get_exchg_pairs(db, exchg_id):
        if not pair.used or not pair.ticker:
            continue
        t1 = get_ticker(db, exchg_id, pair.curr1_id)
        t2 = get_ticker(db, exchg_id, pair.curr2_id)
        if not t1 or not t2:
            continue

        '''
        v1 http://www.cryptsy.com/api.php?method=singlemarketdata&marketid=132
        v2 https://www.cryptsy.com/api/v2/markets/132
        pubapi2.cryptsy.com - (Amsterdam, Netherlands)
        DOGE - 132
        {"success":1,"return":{"markets":{"DOGE":{"marketid":"132","label":"DOGE\/BTC","lasttradeprice":"0.00000071","volume":"102058604.42108892","lasttradetime":"2015-07-07 09:30:31","primaryname":"Dogecoin","primarycode":"DOGE","secondaryname":"BitCoin","secondarycode":"BTC","recenttrades":[{"id":"98009366","time":"2015-07-07 
        '''
        #current.logger.debug(t1 + ';' + t2 + ' pair.ticker: ' + pair.ticker,)

        try:
            #if True:
            #params = {'method': 'singlemarketdata', 'marketid': pair.ticker }
            cryp_url = 'https://' + exchg.url + '/api/v2/markets/' + pair.ticker +'/ticker'
            #current.logger.debug(cryp_url)
            res = fetch(cryp_url)
            res = json.loads(res)
            if type(res) != dict:
                continue
            if not res.get('success'): continue
            if True:
                # v2
                # {"success":true,"data":{"id":"132","bid":4.7e-7,"ask":4.9e-7}}
                res = res['data']
                buy = res['ask']
                sell = res['bid']
            else:
                # v1
                rr = res['return']['markets'].get('DOGE')
                if not rr:
                    continue
                ll = rr['label']
                pair_ll = t1 + '/' + t2
                if ll.lower() != pair_ll.lower():
                    current.logger.info('ll.lower() != pair_ll.lower() ' + ll.lower() + ' ' + pair_ll.lower())
                    continue

                # тут обратные ордера поэтому наоборот
                buy = rr['sellorders'][0]['price']
                sell = rr['buyorders'][0]['price']
            #return dict(buy= buy, sell= sell)
            current.logger.debug('sell: %s, buy: %s' % (sell, buy))
            db_common.store_rates(db, pair, sell, buy)
        except Exception as e:
            current.logger.exception(e)
            continue
    db.commit()

#  с биржи BTC-e.com
## api/3/ticker/btc_usd-btc_rur...
def from_btc_e_3(db,exchg):
    exchg_id = exchg.id
    pairs = []
    for pair in db_common.get_exchg_pairs(db, exchg_id):
        if not pair.used: continue
        t1 = get_ticker(db, exchg_id, pair.curr1_id)
        t2 = get_ticker(db, exchg_id, pair.curr2_id)
        if not t1 or not t2:
            continue

    if t1 and t2:
            pairs.append(t1+'_'+t2)
    pairs = '-'.join(pairs)
    url = 'https://' + exchg.url + '/api/3/ticker/' + pairs
    #url = 'https://btc-e.com/api/3/ticker/' + pairs
    current.logger.info(url)
    try:
        resp = fetch(url)
        res = json.loads(resp)
        for k,v in res.items():
            current.logger.info( k[:3] + ' ' + k[4:]) # v
            curr1 = get_curr(db, exchg_id, k[:3])
            curr2 = get_curr(db, exchg_id, k[4:])
            if not curr1 or not curr2:
                current.logger.info('not curr found for serv rate')
                continue
            pair = db((db.exchg_pairs.curr1_id == curr1.id)
                      & (db.exchg_pairs.curr2_id == curr2.id)).select().first()
            if not pair:
                current.logger.info('pair nor found in get_exchg_pairs')
                continue
            db_common.store_rates(db, pair, v['sell'], v['buy'])
            current.logger.info('updates: %s %s' % (v['sell'], v['buy']))
        db.commit()
        return resp
    except Exception as e:
        msg = "serv_rates %s :: %s" % (exchg.url, e)
        current.logger.info(msg)
        return msg

def get_from_exch(db, exchg):
    if exchg.api_type == 'btc-e_3':
        return from_btc_e_3(db,exchg)
    elif exchg.api_type == 'poloniex':
        return from_poloniex(db, exchg)
    elif exchg.api_type == 'livecoin':
        return from_livecoin(db, exchg)
    elif exchg.api_type == 'cryptsy':
        return from_cryptsy(db, exchg)
    else:
        conn = exch_client.conn(exchg)
        res = from_btc_e_3(db,exchg)
        exch_client.conn_close(exchg)
        return res
    #return conn

def make_cross(db):
    ##### MAKE CROSSES for HARD PRICES
    expired = datetime.datetime.now() - datetime.timedelta(0, 30) # use latest rate
    BTC_CURR, _, _ = db_common.get_currs_by_abbrev(db, 'BTC')
    USD_CURR, _, _ = db_common.get_currs_by_abbrev(db, 'USD')
    exchg_id = 5 # Cross
    count = 0
    for rec in db(db.exchg_pair_bases).select():
        if not rec.hard_price:
            continue

        if rec.curr1_id == BTC_CURR.id or rec.curr2_id == BTC_CURR.id \
                or rec.curr1_id == USD_CURR.id or rec.curr2_id == USD_CURR.id:
            continue

        buy, sell, avrg = rates_lib.get_average_rate_bsa(db, rec.curr1_id, USD_CURR.id, expired)
        if avrg:
            _, _, avrg2 = rates_lib.get_average_rate_bsa(db, rec.curr2_id, USD_CURR.id, expired)
            if avrg2:
                continue
            #current.logger.debug(str(rec.curr1_id) + '/ USD ' + str(avrg) + ', for ' + str(rec.curr2_id) + ' cross: ' + str(avrg * float(rec.hard_price)))
            db_common.store_cross_rates(db, exchg_id, rec.curr2_id, USD_CURR.id, buy * float(rec.hard_price), sell * float(rec.hard_price))
            count += 1
            continue

        buy, sell, avrg = rates_lib.get_average_rate_bsa(db, rec.curr2_id, USD_CURR.id, expired)
        if avrg:
            _, _, avrg2 = rates_lib.get_average_rate_bsa(db, rec.curr1_id, USD_CURR.id, expired)
            if avrg2:
                continue
            #current.logger.debug(str(rec.curr2_id) + '/ USD ' + str(avrg) + ', for ' + str(rec.curr1_id) + ' cross: ' + str(avrg * float(rec.hard_price)))
            db_common.store_cross_rates(db, exchg_id, rec.curr1_id, USD_CURR.id, buy * float(rec.hard_price), sell * float(rec.hard_price))
            count += 1
            continue

        buy, sell, avrg = rates_lib.get_average_rate_bsa(db, BTC_CURR.id, rec.curr1_id, expired)
        if avrg:
            _, _, avrg2 = rates_lib.get_average_rate_bsa(db, BTC_CURR.id, rec.curr2_id, expired)
            if avrg2:
                continue
            #current.logger.debug('BTC /' + str(rec.curr1_id) + ' ' + str(avrg) + ', for' + str(rec.curr2_id) + ' cross: ' + str(avrg * float(rec.hard_price)))
            db_common.store_cross_rates(db, exchg_id, BTC_CURR.id, rec.curr2_id, buy * float(rec.hard_price), sell * float(rec.hard_price))
            count += 1
            continue

        buy, sell, avrg = rates_lib.get_average_rate_bsa(db, BTC_CURR.id, rec.curr2_id, expired)
        if avrg:
            _, _, avrg2 = rates_lib.get_average_rate_bsa(db, BTC_CURR.id, rec.curr1_id, expired)
            if avrg2:
                continue
            #current.logger.debug('BTC /' + str(rec.curr2_id) + ' ' + str(avrg) + ', for' + str(rec.curr1_id) + ' cross: ' + str(avrg * float(rec.hard_price)))
            db_common.store_cross_rates(db, exchg_id, BTC_CURR.id, rec.curr1_id, buy * float(rec.hard_price), sell * float(rec.hard_price))
            count += 1
            continue

        pass

    db.commit()
    return count


# if Interval < 0 use once
def get_rates(db, interval=None):
    interval = interval or 120

    while True:
        # по всем биржам
        for exchg in db(db.exchgs).select():
            if not exchg.used or not exchg.url or len(exchg.url) < 3:
                continue

            current.logger.info(exchg.name)
            if True:
                get_from_exch(db, exchg)
            else:
                # rise error - https://groups.google.com/g/web2py/c/7Dl1lUeotgk/m/GiR89Y_0BAAJ
                threadGetRate = Thread(target=get_from_exch, args=(db, exchg))
                threadGetRate.start()
            pass

        db.commit()
        current.logger.info('\n rates updated - %s' % datetime.datetime.now())

        # make cross rates
        while make_cross(db) > 0:
            #current.logger.debug('next cross')
            pass

        if True:
            # если есть невыплаченные покупки криптовалюты
            clients_lib.notify(db)

        if interval < 0:
            # run once
            return


        current.logger.info('sleep %s sec' % interval)
        sleep(interval)


# если делать вызов как модуля то нужно убрать это иначе неизвестный db
if len(sys.argv) > 1:
    get_rates(db, int(sys.argv[1]))
