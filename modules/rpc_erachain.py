#!/usr/bin/env python
# coding: utf8

# [{u'creator': u'777RHZ9xEcyrBowSpYJYqYyNW4R4gfK28D', u'height': 76064, u'record_type': u'SEND', u'size': 159,
# u'property1': 0, u'property2': 158, u'fee': u'0', u'forgedFee': 0, u'title': u'', u'sub_type_name': u'SEND',
# u'seqNo': u'76064-1', u'type_name': u'SEND', u'balancePos': 1, u'publickey': u'9kxQacdvwBugooBYQ2MxTrhRK37ReQN3AJSKCnSq9Xj9',
# u'version': 0, u'asset': 20, u'type': 31, u'tags': [u'@a:ast', u'@tt31', u'send', u'@a20'], u'timestamp': 1622328667016, u'sequence': 1, u'royaltyFee': 0,
# u'confirmations': 798, u'recipient': u'7KC2LXsD6h29XQqqEa7EpwRhfv89i8imGK', u'invitedFee': 0, u'actionName': u'Transfer to the ownership', u'feePow': 0,
# u'assetKey': 20, u'amount': u'9.174441',
# u'deadLine': 1622328907016, u'signature': u'gd1Jyyh35GNYq7f7qooNKGWzJQ2m7sH56U6bbD4dRY1iwa9d8CBCF4fB6d2bFfBSfsPz4M1YRFm4Uq7cQ5rZMeB'}]

import urllib

from gluon import current
from gluon.contrib import simplejson as json
from decimal import Decimal
import time

import db_common
import crypto_client

def rpc_request(pars, vars=None, test=None, as_json=True):
    if test:
        vars['test_payment'] = True
        vars['test_result'] = test

    if vars:
        vars = urllib.parse.quote(vars)
        # current.logger.debug ('rpc VARS:' + str(vars))
        rq = urllib.request.Request(pars + '?' + vars)
    else:
        rq = urllib.request.Request(pars)

    ##rq.add_header('Authorization', 'Bearer ' + token)
    # return rq
    # платеж в процессе - ожидаем и потом еще раз запросим
    try:
        f = urllib.request.urlopen(rq)
        if as_json:
            r = json.load(f)
        else:
            r = f.read().decode("utf-8")

        # # current.logger.debug ('response - res: %s' % r)
    except Exception as e:
        # или любая ошибка - повтор запроса - там должно выдать ОК
        # current.logger.debug ('YmToConfirm while EXEPTION:', e)
        current.logger.exception('urlopen [%s]' % rq, e)
        return e

    # time.sleep(1)

    return r


def get_xcurr_by_system_token(db, token_system, token_key):
    try:
        token_key = int(token_key)  # ASSET KEY in Erachain
    except Exception as e:
        curr_out, xcurr, _ = db_common.get_currs_by_abbrev(db, token_key)
        return xcurr

    token = db((db.tokens.system_id == token_system.id)
               & (db.tokens.token_key == token_key)).select().first()
    if not token:
        return

    return db(db.xcurrs.as_token == token.id).select().first()


def get_addresses(token_system):
    return rpc_request(token_system.connect_url + "/addresses?password=" + token_system.password)


def get_height(rpc_url):
    return rpc_request(rpc_url + "/blocks/height")


def is_not_valid_addr(rpc_url, address):
    res = rpc_request(rpc_url + "/addresses/validate/" + address)
    return not res


# all tokens for account
def get_assets_balances(token_system, address=None):
    return rpc_request(token_system.connect_url + "/addresses/assets/" + (address or token_system.account))


# one token balance
def get_balance(token_system, address, token=2):
    recs = rpc_request(token_system.connect_url + '/addresses/assetbalance/' + str(token) + '/' + address)
    if type([]) == type(recs):
        return Decimal(recs[0][1])
    return recs


# get transactions/unconfirmedincomes/7F9cZPE1hbzMT21g96U8E1EfMimovJyyJ7
def get_unconf_incomes(rpc_url, address):
    recs = rpc_request(rpc_url + '/transactions/unconfirmedincomes/' + address)
    return recs


def tx_exist_in_chain(rpc_url, txid):
    res = rpc_request(rpc_url + '/transactions/seqno/' + txid, as_json=False)
    if not res or res.startswith('{'):
        return False

    ss = res.split('-')
    return len(ss) == 2 and ss[0].isnumeric() and ss[1].isnumeric()


def get_tx_seq_no(rpc_url, txid):
    return rpc_request(rpc_url + '/transactions/seqno/' + txid, as_json=False)


def get_tx_info(rpc_url, txid):
    return rpc_request(rpc_url + '/transactions/signature/' + txid)


# for precess incomes in serv_block_proc
def parse_tx_fields(rec):
    title = rec.get('title')  ## may be = u''
    return dict(
        creator=rec['creator'],
        recipient=rec['recipient'],
        amount=Decimal(rec['amount']),
        asset=rec['asset'],
        message=rec.get('message', title),
        txid=rec['signature'],
        vout=0,
        block=rec['height'],
        timestamp=rec['timestamp'] / 1000,  # in SEC
        confs=rec['confirmations']
    )


def get_transactions(token_system, from_block=2):
    rpc_url = token_system.connect_url
    addr = token_system.account

    result = []

    height = rpc_request(rpc_url + "/blocks/height")
    try:
        height = int(height)
    except Exception as e:
        return result, None

    i = from_block
    timestamp = time.time()
    tx_count = 0

    while i < height:
        if len(result) > 100 or i - from_block > 10000:
            break

        # for each block too
        tx_count += 10

        if tx_count > 1000:
            tx_count = 0
            if time.time() - timestamp > 10:
                # if time of process more then 10 sec - break!
                break

        i += 1
        url_get = rpc_url + '/transactions/incoming/' + str(i) + '/' + addr + '/decrypt/%s' % token_system.password

        try:
            recs = rpc_request(url_get)
        except Exception as e:
            current.logger.exception(e)
            return result, i - 1

        if type(recs) != type([]):
            current.logger.critical(token_system.name + ' - get_txs %s ERROR: %s' % (url_get, recs))
            break

        recs_count = len(recs)

        if recs_count == 0:
            continue

        # current.logger.debug (token_system.name + ' incomes - height: %s, recs: %s' % (i, len(recs)))

        incomes = []
        for rec in recs:

            ++tx_count

            if rec['type'] != 31:
                # only SEND transactions
                continue
            if 'amount' not in rec:
                # only SEND transactions
                continue
            if 'balancePos' not in rec or rec['balancePos'] != 1:
                # only SEND PROPERTY action
                continue
            if 'backward' in rec:
                # skip BACKWARD
                continue
            if rec.get('title') is '.main.' or rec.get('message') is '.main.':
                ## skip my deposit
                continue

            incomes.append(rec)
            # current.logger.debug('title: ' + rec.get('title') + ' message:' + rec.get('message'))

        result += incomes

    return result, i


def send(curr, xcurr, addr, amo, token_system=None, token=None, title=None, mess=None):

    amo_to_pay = round(float(amo), 8)

    balance = get_balance(token_system, token_system.account, token.token_key)

    if type(balance) != type(Decimal(0)):
        return {'error': balance}, None

    # проверим готовность базы - is lock - и запишем за одно данные
    pars = '/rec_payment/%d/%s/%d/%f/%s?password=%s' % (
        0, token_system.account, token.token_key, amo_to_pay, addr, token_system.password)
    res = rpc_request(token_system.connect_url + pars)

    if type(res) != type({}):
        tx_params = {'from': token_system.account, 'asset': token.token_key, 'amount': amo_to_pay, 'to': addr}
        return {'error': res, 'tx': tx_params}, None

    elif 'error' in res:
        tx_params = res if 'signature' in res else {'from': token_system.account, 'asset': token.token_key, 'amount': amo_to_pay, 'to': addr}
        return {'error': res['error'], 'tx': tx_params}, None

    return {'txid': res['signature'], 'tx': res}, balance - amo
