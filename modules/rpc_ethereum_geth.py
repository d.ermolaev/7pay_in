#!/usr/bin/env python
# coding: utf8

import urllib
import codecs
from time import sleep
from datetime import datetime

from gluon import current
from decimal import Decimal
import decimal

from gluon.contrib import simplejson as json
import time

import db_common

PRECISION = 18


def hex_wei_to_dec(hex_amo):
    decimal.getcontext().prec = 18  # WEI
    return Decimal(int(hex_amo, 0)) * Decimal(1E-18)


def wei_to_dec(amo_wei):
    decimal.getcontext().prec = 18  # WEI
    return Decimal(amo_wei) * Decimal(1E-18)


def eth_to_wei(amo):
    return int(Decimal(amo) * Decimal(1E18))


def eth_to_wei_hex(amo):
    return '%#x' % int(Decimal(amo) * Decimal(1E18))


def mixedcase(hex_value):
    return


# 'id' for SEND ned be UNIQUE!
def rpc_request(rpc_url, method, params=[], test=None):
    if test:
        params['test_result'] = test

    headers = {'Content-Type': 'application/json'}

    # id - only for send by secret channel foe nonce in transit data
    # for local use - id as constant 1 is good
    data = '{"method":"' + method + '", "jsonrpc": "2.0", "params":' + ('%s' % json.dumps(params)) + ', "id": 1}'

    if test:
        current.logger.debug('REQUEST: ' + str(data))

    rq = urllib.request.Request(rpc_url, data.encode("utf-8"), headers=headers)

    try:
        f = urllib.request.urlopen(rq)
        r = json.load(f)
    except Exception as e:
        current.logger.exception(e)
        return '%s' % e

    if test:
        current.logger.debug('RESPONSE: ' + str(r))

    return r


def get_xcurr_by_system_token(db, token_system, token_key):
    token_key = int(token_key)
    if token_key == 1:
        # ETH
        token = db((db.tokens.system_id == token_system.id)
                   & (db.tokens.token_key == token_key)).select().first()
        if not token:
            return

        return db(db.xcurrs.as_token == token.id).select().first()

    curr_out, xcurr, _ = db_common.get_currs_by_abbrev(db, token_key)
    return xcurr


def get_addresses(rpc_url):
    result = rpc_request(rpc_url, "eth_accounts")
    try:
        return result['result']
    except:
        return result


# eth.blockNumber should return the latest synced block number at all times
# see: https://github.com/ethereum/go-ethereum/issues/14338
def get_height(rpc_url):
    result = rpc_request(rpc_url, "eth_blockNumber")
    try:
        return int(result['result'], 0)
    except:
        return result


def is_not_valid_addr(rpc_url, address):
    result = rpc_request(rpc_url, "eth_getBalance", [address, "latest"])
    try:
        return result['error'] != None
    except:
        return False


# balances by token ID = [[0, balance]]
def get_assets_balances(token_system, address):
    result = get_balance(token_system, address)
    try:
        return {
            '1': [[0, result]]  # ETH
        }
    except:
        return result


def get_balance(token_system, address, token=1):
    if token == 1:
        result = rpc_request(token_system.connect_url, "eth_getBalance", [address or token_system.account, "latest"])
        try:
            balance = hex_wei_to_dec(result['result'])
        except:
            return result

        if balance > 1E7:  # in test network
            balance = Decimal(1E7)

        return balance

    return Decimal(0)


# get transactions/unconfirmedincomes/7F9cZPE1hbzMT21g96U8E1EfMimovJyyJ7
def get_unconf_incomes(rpc_url, address):
    ##########  TO and FROM in LOWER CASE !!!!
    address = address.lower()
    incomes = []

    try:
        for rec in get_block(rpc_url, 'pending'):
            if 'to' not in rec or 'value' not in rec or 'input' not in rec or rec['input'] == '0x' \
                    or 'from' not in rec or rec['to'] != address:
                continue

            rec['message'] = codecs.decode(rec['input'][2:], "hex_codec").decode("utf-8")

            incomes.append(rec)

        return incomes
    except Exception as e:
        current.logger.exception(e)
        return "%s" % e


def tx_exist_in_chain(rpc_url, txid):
    res = rpc_request(rpc_url, 'eth_getTransactionReceipt', [txid])
    return res and 'result' in res and res['result'] and 'blockNumber' in res['result'] or False


def get_tx_seq_no(rpc_url, txid):
    res = rpc_request(rpc_url, 'eth_getTransactionReceipt', [txid])
    return res and 'result' in res and res['result'] and 'blockNumber' in res['result'] \
        and str(int(res['result']['blockNumber'], 0)) + '-' + str(int(res['result']['transactionIndex'], 0)) \
        or res



def get_tx_info(rpc_url, txid):
    res = rpc_request(rpc_url, "eth_getTransactionReceipt", [txid])
    return res


def get_block(rpc_url, block):
    res = rpc_request(rpc_url, "eth_getBlockByNumber", ['int' == type(block) and ('%#x' % block) or block, True])
    return res


# for precess incomes in serv_block_proc
'''
[{'blockHash': '0xda5c2a719ad0f12d239acae4983ab8c5c2ed274e3e2647807a9bec8552e31c74', 'blockNumber': '0xc7b16e',
 'from': '0x87025a01969fb46337876d8b73840fa05890be78', 'gas': '0x7ecc', 'gasPrice': '0xc69f8e4c2',
  'maxFeePerGas': '0x10fd0c43c8', 'maxPriorityFeePerGas': '0x59682f00',
   'hash': '0xc440ce011c473dfd3151aebebcf97773fcebbcc119e04005d718325866b3b43e', 
   'input': '0x4d564f4c543a3748654d616e79585357597646394d4857656357367768503250576763794a323367', 
   'nonce': '0x5', 'to': '0xe699ac988f1275d78699d1dea99be0ef15a91f03', 'transactionIndex': '0x93', 
   'value': '0xb1a2bc2ec50000', 'type': '0x2', 'accessList': [], 'chainId': '0x1',
    'v': '0x0', 'r': '0xe80ce83cc364cc270c8de9ba7a6a7a53403aa08b55b958c919cfb745d7da7ceb', 
    's': '0x4eeb9fe40d546188007949038e36926501b942cc7c08f88275ed3b6ca489a7ac', 'block': (13087086,),
     'timestamp': 1629795662, 'confirmations': 5, 'message': 'MVOLT:7HeManyXSWYvF9MHWecW6whP2PWgcyJ23g'}]
'''


def parse_tx_fields(rec):
    return dict(
        creator=rec['from'],
        recipient=rec['to'],
        amount=hex_wei_to_dec(rec['value']),
        asset=1,  # ETH=1
        message=codecs.decode(rec['input'][2:], "hex_codec").decode("utf-8"),
        txid=rec['hash'],
        vout=0,
        block=rec['block'],
        timestamp=rec['timestamp'],
        confs=rec['confirmations']
    )


def get_transactions(token_system, from_block=2):
    rpc_url = token_system.connect_url
    ##conf = token_system.conf

    ##########  TO and FROM in LOWER CASE !!!!
    addr = token_system.account.lower()

    result = []

    res = rpc_request(rpc_url, "eth_blockNumber")
    try:
        height = int(res['result'], 0)
    except Exception as e:
        return result, None

    i = from_block

    timestamp = time.time()
    tx_count = 0

    ## TODO + confirmed HARD
    while i < height:

        # for each block too
        tx_count += 10

        if len(result) > 100 or i - from_block > 10000:
            break

        if tx_count > 1000:
            tx_count = 0
            if time.time() - timestamp > 20:
                # if time of process more then 10 sec - break!
                break

        i += 1

        res = rpc_request(rpc_url, "eth_getBlockByNumber", ['%#x' % i, True])
        try:
            block = res['result']
            recs = block['transactions']
        except Exception as e:
            current.logger.exception(e)
            return result, i - 1

        if type(recs) != type([]):
            current.logger.critical(token_system.name + ' - get_txs %s ERROR: %s' % (rpc_url, recs))
            break

        recs_count = len(recs)

        if recs_count == 0:
            continue

        incomes = []
        for rec in recs:

            ++tx_count

            ##########  TO and FROM in LOWER CASE !!!!
            if 'to' not in rec or 'value' not in rec or 'input' not in rec or rec['input'] == '0x' \
                    or 'from' not in rec or rec['to'] != addr:
                continue

            rec['block'] = i
            rec['timestamp'] = int(block['timestamp'], 0)
            rec['confirmations'] = height - i
            rec['message'] = codecs.decode(rec['input'][2:], "hex_codec").decode("utf-8")

            incomes.append(rec)

        result += incomes

    return result, i


def send(curr, xcurr, toAddr, amo, token_system, nonce=None, token=None, mess=None, sender=None, password=None):
    rpc_url = token_system.connect_url
    sender = sender or token_system.account
    txfee = token_system.txfee

    balance = get_balance(token_system, sender)
    if type(balance) != type(Decimal(0)):
        return {'error': balance}, None

    decimal.getcontext().prec = 18  # WEI
    amo_to_pay = eth_to_wei(amo)  # in WEI
    txfee = int(txfee * Decimal(1E10))  # and x 1+E10 by gasPrice
    gasPrice = int(1E8)
    tx_params = {
        'from': sender,
        'to': toAddr,
        'value': '%#x' % amo_to_pay
    }
    if mess:
        tx_params['data'] = '0x' + (mess.encode("utf-8").hex())

    # take gas tax
    send_params = [tx_params]
    res = rpc_request(token_system.connect_url, 'eth_estimateGas', send_params)
    tx_fee_approx = res['result']

    current.logger.debug('try send: %s[%s] %s, fee: %s' % (amo, curr.abbrev, toAddr, hex_wei_to_dec(tx_fee_approx)))

    password = password or token_system.password

    # NONCE is auto-incremented by NODE. But if transaction not confirmed - need resend with new GAS and same NONCE
    # because all new TXs with big NONCE will not broadcast to network
    if nonce != None:
        tx_params['nonce'] = '%#x' % nonce

    use_old_method = False  # set Price
    use_max_method = True  # set MAX oe node self
    if use_old_method:
        # eth_estimateGas({from: "0xe699ac988f1275d78699D1deA99bE0EF15a91F03", to:
        # "0x87025A01969FB46337876D8b73840Fa05890Be78", value: web3.toWei(0.0012, "ether")})
        tx_params['gas'] = '%#x' % txfee
        # GETH self set::params[0]['gasPrice'] = rpc_request(token_system.connect_url, 'eth_gasPrice')
    elif use_max_method:
        tx_params['maxFeePerGas'] = '0x1000000000'
        # GETH self set: params[0]['maxPriorityFeePerGas'] = rpc_request(token_system.connect_url,
        #  eth_maxPriorityFeePerGas')['result']

    use_personal_send = True
    if use_personal_send:
        send_params.append(password)
    else:
        # for method eth_sendTransaction with [pars] - need unlock before^
        res = rpc_request(rpc_url, "personal_unlockAccount", [sender, password, 3])
        if 'error' in res:
            return res, balance

    current.logger.debug('SEND: amo: %s fee: %s [nonce:%s] %s'
                         % (amo, Decimal(txfee * gasPrice) / Decimal(1E18),
                            nonce, tx_params))

    if use_personal_send:
        res = rpc_request(rpc_url, "personal_sendTransaction", send_params)
    else:
        res = rpc_request(rpc_url, "eth_sendTransaction", send_params)

    if 'result' not in res:
        res['tx'] = tx_params
        return res, None

    elif res['result'] == "0x0":
        # some error
        return {'error': {'code': -5, 'message': res}, 'tx': tx_params}, None

    return {'txid': res['result'], 'tx': tx_params}, balance - amo - hex_wei_to_dec(tx_fee_approx)
